from django.shortcuts import render
from alibaba.serializers import *
from alibaba.models import *
from rest_framework import viewsets, permissions

# Create your views here.

class ItemViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows items to be viewed or edited.
    """
    queryset = Item.objects.all()
    serializer_class = ItemSerializer
    # permission_classes = [permissions.IsAuthenticated]
