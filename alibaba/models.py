from django.db import models

# Create your models here.

class Item(models.Model):
    class Meta:
            db_table = 'item'

    name = models.CharField(max_length=255, null=False)
    price = models.CharField(max_length=100, null=False)
    min_order = models.CharField(max_length=100, null=False)
    img = models.CharField(max_length=200, null=False)    
    description = models.TextField(null=False)    
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.name

