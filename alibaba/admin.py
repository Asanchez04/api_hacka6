from django.contrib import admin
from django.contrib.auth.models import Permission
from alibaba.models import *

# Register your models here.

@admin.register(Item)
class ItemAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'price',
        'img',
        'min_order',
        'description',
        'is_active',
    )
    search_fields = (
        'name',
    )
