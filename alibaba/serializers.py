from rest_framework import serializers
from alibaba.models import *

class ItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = Item
        fields = ['id', 'name', 'price','min_order', 'description', 'img', 'is_active']

        